<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<html>

    <style type="text/css">
         .header {
             background-color: black;
                color: azure;
                height: 60px;
         }
         .customButton {
                background-color: green;
                color: aliceblue;
                width: 200px;
                height: 30px;
                position: center;
                cursor: pointer;
         }
    </style>

    <head>
        <title>Edit Project</title>
    </head>

    <header class="header"></header>

    <body style="text-align: center;
            background-image:
            url(https://i.ytimg.com/vi/VsRXeSb9nlw/maxresdefault.jpg)">
        <h1>EDIT TASK</h1>

        <form:form method="POST" action="/tasks/edit?projectId=${task.project.id}&taskId=${task.id}"
                   modelAttribute="task">
            <div>
                <form:label path="name">Name</form:label>
            </div>
            <div style="margin-top: 10px">
                <form:input path="name"/>
            </div>
            <div style="margin-top: 10px">
                <form:label path="description">Description</form:label>
            </div>
            <div style="margin-top: 10px">
                <form:input path="description"/>
            </div>
            <div>
                <br>
                <input type="submit" value="UPDATE" class="customButton">
            </div>
        </form:form>

    </body>

</html>
