<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
    </style>

    <head>
        <title>Create Task</title>
    </head>

    <header class="header"></header>

    <body style="text-align: center">
        <h1>CREATE TASK</h1>

        <form:form method="POST" action="/tasks/create?projectId=${projectId}" modelAttribute="task">
            <div style="margin-top: 10px">
                <form:label path="name">Name</form:label>
            </div>
            <div style="margin-top: 10px">
                <form:input path="name"/>
            </div>
            <div style="margin-top: 10px">
                <form:label path="description">Description</form:label>
            </div>
            <div style="margin-top: 10px">
                <form:input path="description"/>
            </div>

            <div>
                <br>
                <input type="submit" value="Create" class="customButton">
            </div>
        </form:form>
    </body>

</html>
