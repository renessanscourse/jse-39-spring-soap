package ru.ovechkin.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface ArgumentConst {

    @NotNull
    String ARG_HELP = "-h";

    @NotNull
    String ARG_INFO = "-i";

    @NotNull
    String ARG_VERSION = "-v";

    @NotNull
    String ARG_ABOUT = "-a";

    @NotNull
    String ARG_ARGUMENTS = "-ar";

    @NotNull
    String ARG_COMMANDS = "-c";

}