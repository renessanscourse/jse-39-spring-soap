package ru.ovechkin.tm.soap.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/TaskEndpoint?wsdl
 */
@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    public List<Task> findAll(@WebParam(name = "projectId") @Nullable String projectId) {
        return taskService.findAll(projectId);
    }

    @WebMethod
    public void save(@WebParam(name = "task") @Nullable Task task) {
        taskService.save(task);
    }

    @WebMethod
    public void removeById(@WebParam(name = "taskId") @Nullable String taskId) {
        taskService.removeById(taskId);
    }

    @NotNull
    @WebMethod
    public Task findById(@WebParam(name = "taskId") @Nullable String taskId) {
        return taskService.findById(taskId);
    }

    @WebMethod
    public void updateById(
            @WebParam(name = "taskId") @Nullable String taskId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "task") @Nullable Task task
    ) {
        taskService.updateById(taskId, projectId, task);
    }

}