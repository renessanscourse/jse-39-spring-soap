package ru.ovechkin.tm.soap.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/ProjectEndpoint?wsdl
 */
@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @WebMethod
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @WebMethod
    public void save(@WebParam(name = "project") @Nullable Project project) {
        projectService.save(project);
    }

    @WebMethod
    public void removeById(@WebParam(name = "projectId") @Nullable String projectId) {
        projectService.removeById(projectId);
    }

    @WebMethod
    public Project findById(@WebParam(name = "projectId") @Nullable String projectId) {
        return projectService.findById(projectId);
    }

    @WebMethod
    public void updateById(
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "project") @Nullable Project project
    ) {
        projectService.updateById(id, project);
    }

}
