package ru.ovechkin.tm.exeption.empty;

public class LoginEmptyException extends RuntimeException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}